# Relative league tables

A visualisation of a few European leagues showing clubs' relative positions based on points rather than rank. [See live version](https://guushoekman.com/relative-league-tables).

![Screenshot](https://i.imgur.com/74uDEnj.png)

Each dot represents a team, colour coded by European qualification / relegation. Its position is based on the club's points rather than rank.

The club at the top of the league is at the top of its column, regardless of the number of points. Each row represents a point, meaning the larger the white space between teams the bigger the gap in points.

Data provided by the [football-data.org API](https://www.football-data.org/).