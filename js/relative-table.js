var leagues = [
	{
		"code": de,
		"code_string": "de",
		"teams": 18,
		"cl_start": 0,
		"cl_end": 4,
		"el_start": 4,
		"el_end": 6,
		"european_playoffs_start": null,
		"european_playoffs_end": null,
		"relegation_playoffs_start": 15,
		"relegation_playoffs_end": 16,
		"relegation_start": 16,
		"relegation_end": 18,
	},
	{
		"code": en,
		"code_string": "en",
		"teams": 20,
		"cl_start": 0,
		"cl_end": 4,
		"el_start": 4,
		"el_end": 5,
		"european_playoffs_start": null,
		"european_playoffs_end": null,
		"relegation_playoffs_start": null,
		"relegation_playoffs_end": null,
		"relegation_start": 17,
		"relegation_end": 20,
	},
	{
		"code": es,
		"code_string": "es",
		"teams": 20,
		"cl_start": 0,
		"cl_end": 4,
		"el_start": 4,
		"el_end": 6,
		"european_playoffs_start": null,
		"european_playoffs_end": null,
		"relegation_playoffs_start": null,
		"relegation_playoffs_end": null,
		"relegation_start": 17,
		"relegation_end": 20,
	},
	{
		"code": fr,
		"code_string": "fr",
		"teams": 20,
		"cl_start": 0,
		"cl_end": 3,
		"el_start": 3,
		"el_end": 4,
		"european_playoffs_start": null,
		"european_playoffs_end": null,
		"relegation_playoffs_start": 17,
		"relegation_playoffs_end": 18,
		"relegation_start": 18,
		"relegation_end": 20,
	},
	{
		"code": it,
		"code_string": "it",
		"teams": 20,
		"cl_start": 0,
		"cl_end": 4,
		"el_start": 4,
		"el_end": 6,
		"european_playoffs_start": null,
		"european_playoffs_end": null,
		"relegation_playoffs_start": null,
		"relegation_playoffs_end": null,
		"relegation_start": 17,
		"relegation_end": 20,
	},
	{
		"code": nl,
		"code_string": "nl",
		"teams": 18,
		"cl_start": 0,
		"cl_end": 2,
		"el_start": 2,
		"el_end": 3,
		"european_playoffs_start": 3,
		"european_playoffs_end": 7,
		"relegation_playoffs_start": 15,
		"relegation_playoffs_end": 17,
		"relegation_start": 17,
		"relegation_end": 18,
	},
	{
		"code": pt,
		"code_string": "pt",
		"teams": 18,
		"cl_start": 0,
		"cl_end": 2,
		"el_start": 2,
		"el_end": 4,
		"european_playoffs_start": null,
		"european_playoffs_end": null,
		"relegation_playoffs_start": null,
		"relegation_playoffs_end": null,
		"relegation_start": 15,
		"relegation_end": 18,
	}
]

// colours
var champions_league = "#4daf4a";
var europa_league = "#377eb8";
var european_playoffs = "#984ea3";
var relegation_playoffs = "#ff7f00";
var relegation = "#e41a1c";

// last updated global
var lastUpdated = new Date(2015, 11, 20);

var mostPointsAllLeagues = 0;
var fewestPointsAllLeagues = 100;

$(leagues).each(function() {
	var code = this["code"];
  var teams = code.standings[0].table;
	var number_teams = this["teams"];

	if (teams[0].points > mostPointsAllLeagues) {
		mostPointsAllLeagues = teams[0].points
	};

	if (teams[number_teams - 1].points < fewestPointsAllLeagues) {
		fewestPointsAllLeagues = teams[number_teams - 1].points
	};
});

// relative table
$(leagues).each(function() {
	var code = this["code"];
	var code_string = this["code_string"];
	var number_teams = this["teams"];

	// get last updated
	var lastUpdatedLeague = new Date(this.code.competition.lastUpdated);
	if (lastUpdatedLeague > lastUpdated) {
		lastUpdated = lastUpdatedLeague
	};

	// create table for each competition
	$("<div class='column'><table class='is-fullwidth table " + code_string + "'><thead><tr><th class='header-teams tooltip' data-tooltip='" + this.code.competition.name + "'><img src='img/" + code_string + ".svg'></th></tr></thead><tbody></tbody></table></div>").appendTo(".tables");
  var teams = code.standings[0].table;
  var mostPoints = teams[0].points;
  var fewestPoints = teams[number_teams - 1].points;

  for (var i = mostPointsAllLeagues; i >= fewestPointsAllLeagues; i--) {
    $("<tr id='" + code_string + "-" + i + "'><td class='teams'><div class='circle-wrap'></div></td></tr>").appendTo("." + code_string + ".table tbody");
  };

  // add each team to position based on points
  $.each(teams, function() {
    var team = $(this)[0];
    $("<span class='team-circle tooltip' data-tooltip='" + team.position + ". " + team.team.name + " (" + team.points + " points)'></span>").appendTo($("#" + code_string + "-" + team.points + " .teams .circle-wrap"))
  });

  $("." + code_string + " .team-circle").slice(this["cl_start"], this["cl_end"]).css("background-color", champions_league);
  $("." + code_string + " .team-circle").slice(this["el_start"], this["el_end"]).css("background-color", europa_league);
  $("." + code_string + " .team-circle").slice(this["european_playoffs_start"], this["european_playoffs_end"]).css("background-color", european_playoffs);
  $("." + code_string + " .team-circle").slice(this["relegation_playoffs_start"], this["relegation_playoffs_end"]).css("background-color", relegation_playoffs);
  $("." + code_string + " .team-circle").slice(this["relegation_start"], this["relegation_end"]).css("background-color", relegation);
});

// add most and fewest points
$(".table.de").parent().before("<div class='column points'><table class='is-fullwidth table points'><thead><tr><th class='header-points'><img src='img/points.svg'></th></tr></thead><tbody></tbody></table></div>");
for (var i = mostPointsAllLeagues; i >= fewestPointsAllLeagues; i--) {
  $("<tr id='points-" + i + "'><td class='teams'><div class='points-wrap'></div></td></tr>").appendTo(".points.table tbody");
};
$("<span class='listed-points'>" + mostPointsAllLeagues + "</span>").appendTo("#points-" + mostPointsAllLeagues + " .points-wrap");
$("<span class='listed-points'>" + fewestPointsAllLeagues + "</span>").appendTo("#points-" + fewestPointsAllLeagues + " .points-wrap");

// show last updated
var lastUpdatedString = lastUpdated.toLocaleString('en-GB', {timeZone: 'UTC', timeZoneName: 'short', day: '2-digit', month: 'short', hour: '2-digit', minute: '2-digit'});
$(".last-update").text(lastUpdatedString)

// show grid
function showGrid() {
	$(".columns.tables").toggleClass("is-grid");
}

// show explanation on mobile
function explain() {
	$("p").removeClass("is-hidden-mobile");
	$("#explain").hide();
}

window.onload = function(){
	var tableHeaderHeight = $(".table.pt thead").height();
	$(".table.points thead").height(tableHeaderHeight);
};